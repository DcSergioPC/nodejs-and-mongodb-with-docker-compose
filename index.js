const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');

const app = express();

// MongoDB connection
mongoose.connect('mongodb://poli:password@mongopoli:27017/recipes?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))


//middleware
app.use(express.json());

app.get('/', (req, res) =>{
    res.send("Hola Mundo");
});

app.post('/new_user', (req, res) =>{
    const user = userSchema(req.body);
    user.save()
    .then( (data) => res.json(data))
    .catch( (error) => res.send(error));
});

app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then( (data) => res.json(data))
    .catch( (error) => res.send(error));
})

app.post('/rate', (req,res) => {
    const { recipeId, userId, rating } = req.body
    recipeSchema.updateOne(
        { _id: recipeId },
        [
            // {$set: {ratings: {$concatArrays: { user: userId, rating: rating }}}},
            {$set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
            // {}
        ]
    ) 
    .then((data) => res.json(data))
    .catch((error) => {
        console.log(error)
        res.send(error)
    });
})

app.get('/recipes', (req,res) => {
    const {userId} = req.body
        recipeSchema
            .find({userId: userId})
            .then((data) => res.json(data))
            .catch((error) => res.send({message: error}))
})

app.get('/recipesbyingredient', (req,res) => {
    const { ingredients } = req.body
    const nameIngredients = ingredients.map(element => element.name)

    recipeSchema
        // .find( { $expr: { $setIsSubset: [ nameIngredients, '$ingredients'.map(element =>element.name) ] } })
        .find( { $expr: { $setIsSubset: [ {$map: { input: "$ingredients", as: "e", in: "$$e.name" }}, nameIngredients ] } })
        .then((data) => res.json(data))
        .catch((error) => res.send({message: error}))
})


app.listen(3000, () => console.log("escuchando en puerto 3000"));